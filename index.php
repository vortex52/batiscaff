<?php require_once('setting.php');?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="vendor/fontello/css/fontello.css">
        <link rel="stylesheet" href="vendor/OwlCarousel2-2.2.1/css/owl.carousel.min.css">
        <link rel="stylesheet" href="vendor/OwlCarousel2-2.2.1/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class="main-wrapper">
            <div class="head-social-block">
            	<div class="social-block">
	            	<a title="vkontakte" target="blanc" href="https://vk.com/batiskaf_kstovo"><i class="demo-icon icon-vkontakte-1"></i></a>
					<a title="instagram" target="blanc" href="https://www.instagram.com"><i class="demo-icon icon-instagram"></i></a>
					<a title="twitter" target="blanc" href="https://twitter.com/"><i class="demo-icon icon-twitter"></i></a>
					<a title="facebook" target="blanc" href="https://ru-ru.facebook.com/"><i class="demo-icon icon-facebook"></i></a>
            	</div>
            	<hr>                
            </div>

            <div class="head-contact-section">
                <div class="left-block">
                    <div class="phone-block">
                        <i class="demo-icon icon-phone"></i>
                        +7(920)250-31-90
                    </div> 
                     <div class="email-block">
                        <i class="demo-icon icon-email"></i>
                        aireen@yandex.ru
                    </div> 

                </div>
                <div class="logo">
                    <div class="log-text-1">Первое детское кафе</div>
                    <div class="log-text-2">Батискаф</div>
                    
                </div>
                <div class="right-block">
                    <i class="demo-icon icon-clock-1"></i>
                    Пн-Пт 9:00-21:00<br/>
                    Пт-Сб 9:00-23:00
                </div>
            </div>

            <div class="owl-carousel owl-theme">
                <div><img src="img/slider-1.jpg"></div>
                <div><img src="img/slider-2.jpg"></div>
                <div><img src="img/slider-3.jpg"></div>
            </div>


            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>


          
           
        </div>

        <script src="js/vendor/jquery-1.12.0.min.js"></script>       
        <script src="js/plugins.js"></script>
        <script src="vendor/OwlCarousel2-2.2.1/js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <!-- <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script> -->
    </body>
</html>
