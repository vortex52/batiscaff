$(document).ready(function(){

  $(".owl-carousel").owlCarousel({ 
	dots : true,	
	autoplay : true,
	autoplayHoverPause : true,
	loop : true,
	autoplaySpeed : 600,
	smartSpeed : 600,
	dotsSpeed : 600, 
	items : 1	
 });

});

